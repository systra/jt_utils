-module(jt_utils).

-export([get_env/1,
         get_env/2,
         get_env/3,
         iso_timestamp/0,
         datetime_to_iso/1,
         datetime_ms/0,
         datetime_ms/1,
         iso_to_datetime/1,
         iso_to_epoch/1,
         epoch_to_datetime/1,
         epoch_to_iso/1,
         new_id/0,
         parse_url/1]).

get_env(App) ->
    application:get_all_env(App).

get_env(App, Key) ->
    get_env(App, Key, undefined).

get_env(App, Key, Default) ->
    case application:get_env(App, Key) of
        {ok, Value} ->
            Value;
        _ ->
            Default
    end.

iso_timestamp() ->
     Date = calendar:now_to_local_time(os:timestamp()),
     datetime_to_iso(Date).

datetime_to_iso({{Y, M, D}, {H, Mi, S}}) ->
    erlang:list_to_binary(io_lib:format("~4.10.0B-~2.10.0B-~2.10.0B ~2.10.0B:~2.10.0B:~2.10.0B", [Y, M, D, H, Mi, S])).

iso_to_datetime(Iso) when is_binary(Iso) ->
    iso_to_datetime(erlang:binary_to_list(Iso));
iso_to_datetime(Iso) when is_list(Iso) ->
    {ok, [Year, Month, Day, Hour, Min, Sec], _} = io_lib:fread("~d-~d-~d ~d:~d:~d", Iso),
    {{Year, Month, Day}, {Hour, Min, Sec}}.

iso_to_epoch(Iso) ->
    DT = iso_to_datetime(Iso),
    calendar:datetime_to_gregorian_seconds(DT).

epoch_to_datetime(V) ->
    calendar:gregorian_seconds_to_datetime(V+719528*24*3600).

epoch_to_iso(V) ->
    D = epoch_to_datetime(V),
    datetime_to_iso(D).

datetime_ms() ->
    datetime_ms(os:timestamp()).

datetime_ms(Now) ->
    {_, _, Micro} = Now,
    {Date, {Hours, Minutes, Seconds}} = calendar:now_to_local_time(Now),
    {Date, {Hours, Minutes, Seconds, Micro div 1000 rem 1000}}.

parse_url(Url) ->
  {ok, Regex} = re:compile("^((?<scheme>[^:]+):)?//((?<user>[^:]+)(:(?<pass>[^@]+))?@)?(?<host>[^:]+)(:(?<port>\\d+))?/(?<path>[^?]+)(\\?(?<q>[^#]*))?(#(?<fragment>.*))?$"),
  Names = [scheme, user, pass, host, port, path, q, fragment],
  case re:run(Url, Regex, [{capture, Names, binary}]) of
    {match, Captured} ->
      {ok, lists:zip(Names, Captured)}
  ; nomatch ->
      {error, []}
  end.

% Initialize before use:
% {X, Y, Z} = now(),
% random:seed(X, Y, Z),
new_id() ->
    Initial = random:uniform(62) - 1,
    new_id(<<Initial>>, 8).
new_id(Bin, 0) ->
    Chars = <<"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890">>,
    << <<(binary_part(Chars, B, 1))/binary>> || <<B>> <= Bin >>;
new_id(Bin, Rem) ->
    Next = random:uniform(62) - 1,
    new_id(<<Bin/binary, Next>>, Rem - 1).

